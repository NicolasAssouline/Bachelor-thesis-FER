from PYTHON import Training

TRAIN_DATA_PATH = \
    "C:\\Users\\Nikola\\Desktop\\FAKS\\6. SEMESTAR\\ZAVRSNI RAD\\Zavrsni_conda_3\\assets\\resumes\\traindata.json"

formatted_data = Training.Trainer.convert_data_to_spacy_format(TRAIN_DATA_PATH)

i = 1
for text, annotations in formatted_data:
    target_file = open("resume"+str(i)+".txt", "w", encoding="utf8")
    target_file.write(text)
    target_file.close()
    i += 1
