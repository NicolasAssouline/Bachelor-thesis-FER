import json
import spacy
import os

DEFAULT_MODEL_PATH = "assets/model/spacy_model.model"


class Trainer:
    '''
    Utility class for easier training of a spacy model
    '''

    def __init__(self, path_to_model=None, training_data_directory=None):
        '''
        If path_to_model is set, loads an existing model
        If training_data_filepath is set, trains a new model and saves it to DEFAULT_MODEL_PATH

        :param path_to_model: A path to an existing spacy model that is going to be used to process data
        :param training_data_directory: A path to the folder containing training data in JSON format
        '''
        # if a new model is to be trained, training data must be provided
        if training_data_directory is not None:
            new_model = True
        else:
            new_model = False

        if path_to_model is not None:
            if not os.path.isfile(path_to_model):
                raise ValueError("The provided path does not point to a valid file")
            global DEFAULT_MODEL_PATH
            DEFAULT_MODEL_PATH = path_to_model

        self.nlp = spacy.blank('en', disable=["parser", "tagger"])

        if 'ner' not in self.nlp.pipe_names:
            self.spacy_pipe = self.nlp.create_pipe('ner')
            self.nlp.add_pipe(self.spacy_pipe)
        else:
            for (name, pipe) in self.nlp.pipeline:
                if name == 'ner':
                    self.spacy_pipe = pipe

        # load the spacy model if it exists
        if new_model or not os.path.isfile(DEFAULT_MODEL_PATH):
            formatted_training_data = Trainer.convert_data_to_spacy_format(training_data_directory)
            self.train_spacy(formatted_training_data)
        else:
            spacy_model = open(DEFAULT_MODEL_PATH, "rb")
            byte_contents = spacy_model.read()
            self.nlp.from_bytes(byte_contents)

    def eval(self, input_object):
        '''
        Evaluates the contents of the provided object using the internal spacy model

        :param input_object: either a path to a file that can be read, or raw text
        :return: a document object containing the parsed information
        '''

        if os.path.isfile(input_object):
            input_file = open(input_object, "r", encoding="utf8")
            return self.nlp(input_file.read())
        elif isinstance(input_object, str):
            return self.nlp(input_object)
        else:
            raise TypeError("The provided object must be either a path to a readable file or raw text")

    def train_spacy(self, formatted_training_data, iterations=15):
        '''
        Utility method for spacy model training.
        Saves the created model to DEFAULT_MODEL_PATH for easier retrieval

        :param formatted_training_data: training data formatted into spacy format
        :param iterations: the number of cycles the model will be trained for
        '''

        # add all labels
        labels = set()
        for text, annotations in formatted_training_data:
            for entity in annotations.get('entities'):
                labels.add(entity[2])

        for label in labels:
            self.spacy_pipe.add_label(label)

        # begin training
        optimizer = self.nlp.begin_training()
        for iteration_num in range(iterations):
            print("Iteration ", iteration_num)

            i = 1
            for text, annotations in formatted_training_data:
                print("  Processing text ", i)
                i += 1

                self.nlp.update([text], [annotations], drop=0.02, sgd=optimizer)

        # save the created model
        print("Saved the created model to ", DEFAULT_MODEL_PATH)
        file = open(DEFAULT_MODEL_PATH, "wb")
        file.write(self.nlp.to_bytes())

    @staticmethod
    def convert_data_to_spacy_format(training_data_directory):
        if not os.path.isdir(training_data_directory):
            raise ValueError("The provided argument must point to a directory containing annotated resumes")

        training_data = []
        # with open(training_data_directory, 'r', encoding="utf8") as f:
        #     lines = f.readlines()

        for filename in os.listdir(training_data_directory):
            if ".json" not in filename:
                continue

            print("Converting file ", filename, " to spacy format")
            with open(training_data_directory + "/" + filename, 'r', encoding="utf8") as f:
                contents = f.read()
                data = json.loads(contents)

            text = data['content']
            entities = []
            for annotation in data['annotation']:
                point = annotation['points'][0]
                labels = annotation['label']
                if not isinstance(labels, list):
                    labels = [labels]

                for label in labels:
                    entities.append((point['start'], point['end'] + 1, label))

            training_data.append((text, {"entities": entities}))

        return training_data
