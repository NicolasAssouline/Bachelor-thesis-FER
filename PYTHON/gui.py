from tkinter import *
from tkinter import filedialog
from tkinter import ttk
from tkinter import messagebox
from PYTHON import Training as Training
from spacy import displacy
import os
import webbrowser


class GUI:
    '''
    A simple interface for easier presentation of extracted data
    '''

    def __init__(self):

        # the model that is going to be used for data processing
        self.trainer = Training.Trainer()
        global DEFAULT_MODEL_PATH
        DEFAULT_MODEL_PATH = Training.DEFAULT_MODEL_PATH
        self.current_doc = None

        # set up the UI
        self.root = Tk()
        self.root.title("Resume data extractor")
        self.root.minsize(700, 500)
        self.root.configure(background="#ffb347")
        GUI.__center__(self.root)

        def on_close():
            if os.path.exists("output.html"):
                os.remove("output.html")
            self.root.destroy()

        self.root.protocol("WM_DELETE_WINDOW", on_close)

        top_frame = Frame(self.root, width=700, bg="#ffb347", pady=5)
        top_frame.grid(row=0, sticky="new")
        Label(top_frame, text="Input the desired file: ").grid(row=0, column=0, padx=10)

        entrytext = StringVar()
        entrytext.set("")
        entry = Entry(top_frame, textvariable=entrytext, bg="bisque")
        entry.grid(row=0, column=1, sticky="ew", padx=10)

        tab_frame = Frame(self.root, width=100, height=100)
        tab_frame.grid(row=1, sticky="nsew", padx=5, pady=(0, 5))

        Grid.rowconfigure(self.root, 1, weight=2)
        Grid.rowconfigure(tab_frame, 0, weight=2)
        Grid.columnconfigure(tab_frame, 0, weight=1)

        tabbedpane = ttk.Notebook(tab_frame, height=200, width=100)
        ttk.Style().configure("TNotebook", background="#ffb347")
        tabbedpane.grid(row=0, sticky="nsew")

        browsebutton = Button(top_frame, text="Browse")
        browsebutton.grid(row=0, column=2)

        def set_path(event):
            file_path = filedialog.askopenfilename(initialdir="./assets/resumes",
                                                   title="Select file",
                                                   filetypes=(("Text files", "*.txt"), ("all files", "*.*")))

            if file_path is not "":
                entrytext.set(os.path.relpath(file_path))

        browsebutton.bind("<ButtonRelease-1>", set_path)

        runbutton = Button(top_frame, text="Run")
        runbutton.grid(row=0, column=3, padx=10)

        def render(event):
            if self.current_doc is not None:
                # displacy.serve(self.current_doc, style="ent", port=5000)
                html = displacy.render(self.current_doc, style="ent")
                html = '''
<!DOCTYPE html>		
	<html lang="en">		
	    <head>		
	        <title>Results</title>		
	    </head>		
			
	    <body style="font-size: 16px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; padding: 4rem 2rem; direction: ltr">\n		
<figure style="margin-bottom: 6rem">\n''' + html + '''	
	</figure>		
	</body>		
	</html>'''

                open("output.html", "w", encoding="utf-8").write(html)
                webbrowser.open_new("output.html")

        render_button = Button(top_frame, text="Render")
        render_button.grid(row=0, column=4, padx=(0, 10))
        render_button.bind("<ButtonRelease-1>", render)

        def run(event):
            input_path = entrytext.get()
            if not os.path.isfile(input_path):
                messagebox.showerror("Input error",
                                     "The provided path is not a file that exists or can be read")
            doc = self.trainer.eval(input_path)
            self.current_doc = doc

            for child in tabbedpane.tabs():
                tabbedpane.forget(child)

            labels = set()
            mappings = {}
            for entity in doc.ents:
                labels.add(entity.label_)

            for label in labels:
                mappings[label] = []

            for entity in doc.ents:
                mappings[entity.label_].append(entity)

            keys = list(mappings.keys())
            keys.sort()

            keys.append("FULL TEXT")
            mappings["FULL TEXT"] = [doc.text]

            for key in keys:
                frame = Frame(tabbedpane)

                all_entries = set()
                text = ""
                for entry in mappings[key]:
                    all_entries.add(str(entry))

                textbox = Text(frame, bg="bisque")

                # disable input in results box
                textbox.insert(INSERT, "\n".join(all_entries).strip())
                textbox.bind("<Key>", lambda e: "break")

                textbox.grid(sticky="nsew")
                Grid.rowconfigure(frame, 0, weight=1)
                Grid.columnconfigure(frame, 0, weight=1)

                tabbedpane.add(frame, text=key.upper())

        entry.bind("<Return>", run)
        runbutton.bind("<ButtonRelease-1>", run)

        # Grid.rowconfigure(self.root, 1, weight=1)
        Grid.columnconfigure(self.root, 0, weight=1)
        Grid.columnconfigure(top_frame, 1, weight=1)

    @staticmethod
    def __center__(win: Tk):
        '''
        Centers the provided window to the center of the screen
        '''
        win.update_idletasks()
        width = win.winfo_width()
        height = win.winfo_height()
        x = (win.winfo_screenwidth() // 2) - (width // 2)
        y = (win.winfo_screenheight() // 2) - (height // 2)
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))

    def show(self):
        self.root.mainloop()


if __name__ == '__main__':
    gui = GUI()
    gui.show()
