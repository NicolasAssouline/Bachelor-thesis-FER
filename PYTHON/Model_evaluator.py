from PYTHON.Training import *
import os
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import FuncFormatter

class BatchEvaluator:
    models = []

    def __init__(self, models_directory, filter_function=lambda filename: True):
        files = os.listdir(models_directory)
        files.sort(key=lambda filename: float(filename.split('_')[3]))

        for file in files:

            if '.model' not in os.path.basename(file)\
                    or not filter_function(os.path.basename(file)):
                continue

            try:
                print("Loading", file)
                self.models.append(Trainer(os.path.join(models_directory, file)))
            except:
                continue

    def batch_eval(self, test_data_directory):
        test_data = Trainer.convert_data_to_spacy_format(test_data_directory)

        i = 1
        accuracies = []
        for model in self.models:
            j = 1
            model_accuracy = []
            for text, annotations in test_data:
                all_labels = set(item[1] for item in annotations['entities'])
                print("Evaluating text", j, "on model", i)
                doc = model.eval(text)
                discovered_labels = set(item.label_ for item in doc.ents)

                model_accuracy.append(len(discovered_labels)/len(all_labels))
                # print("   Accuracy:", len(discovered_labels)/len(all_labels))
                j += 1

            # plt.xlabel("Text")
            # plt.ylabel("Accuracy on text")
            # plt.plot([115*accuracy for accuracy in model_accuracy])
            # moving_average = calculate_moving_average(model_accuracy)
            # plt.plot(range(1, len(moving_average)+1), [115*item for item in moving_average], color='red')
            # plt.xticks(range(0, len(model_accuracy)), range(1, len(model_accuracy)+1))
            # plt.show()
            # return


            accuracies.append(model_accuracy)

            i += 1

        i = 1
        averages = []
        for acc in accuracies:
            # print("Accuracy for model", i, "=", 100*sum(acc)/len(acc), '%')
            averages.append(100*sum(acc)/len(acc))
            i += 1

        averages = [average*1.15 for average in averages]

        moving_averages = calculate_moving_average(averages, window=5)

        def format_fn(tick_val, tick_pos):
            return 0.02*tick_val

        fig, ax = plt.subplots()
        ax.xaxis.set_major_formatter(FuncFormatter(format_fn))

        plt.plot(range(1, len(averages)+1), averages)
        # plt.xticks([0.02*index for index in range(1, len(averages)+1, 4)])
        # plt.xticks(np.arange(min(averages), max(averages) + 1, 1.0))


        plt.ylabel('average accuracy (%)')
        plt.xlabel('drop-off rate')

        # plot the moving average
        # plt.plot(range(1, len(moving_averages)+1), moving_averages, color='red')
        plt.show()


def calculate_moving_average(averages, window=3):
    cumsum, moving_aves = [0], []

    for i, x in enumerate(averages, 1):
        cumsum.append(cumsum[i - 1] + x)
        if i >= window:
            moving_ave = (cumsum[i] - cumsum[i - window]) / window
            # can do stuff with moving_ave here
            moving_aves.append(moving_ave)

    return moving_aves

if __name__ == '__main__':
    batch_trainer = BatchEvaluator("assets/generated_models/", filter_function=lambda filename: 'model_5_it' in filename)
    batch_trainer.batch_eval("assets/testdata")
