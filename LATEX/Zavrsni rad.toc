\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}% 
\contentsline {chapter}{\numberline {2}Strojno u\IeC {\v c}enje}{2}% 
\contentsline {section}{\numberline {2.1}Povijesni pregled}{2}% 
\contentsline {section}{\numberline {2.2}Pristupi strojnom u\IeC {\v c}enju}{3}% 
\contentsline {subsection}{\numberline {2.2.1}Nenadzirano u\IeC {\v c}enje}{3}% 
\contentsline {subsection}{\numberline {2.2.2}Nadzirano u\IeC {\v c}enje}{3}% 
\contentsline {subsection}{\numberline {2.2.3}Djelomi\IeC {\v c}no nadzirano u\IeC {\v c}enje}{3}% 
\contentsline {subsection}{\numberline {2.2.4}Potporno u\IeC {\v c}enje}{4}% 
\contentsline {section}{\numberline {2.3}Obrada prirodnog jezika}{4}% 
\contentsline {chapter}{\numberline {3}Kori\IeC {\v s}tene komponente}{6}% 
\contentsline {section}{\numberline {3.1}Python}{6}% 
\contentsline {section}{\numberline {3.2}SpaCy}{6}% 
\contentsline {chapter}{\numberline {4}Implementacija}{8}% 
\contentsline {section}{\numberline {4.1}Generiranje modela}{8}% 
\contentsline {section}{\numberline {4.2}Obrada podataka}{9}% 
\contentsline {chapter}{\numberline {5}Razvijena aplikacija}{10}% 
\contentsline {chapter}{\numberline {6}Rezultati}{14}% 
\contentsline {chapter}{\numberline {7}Kratka analiza to\IeC {\v c}nosti modela}{17}% 
\contentsline {chapter}{\numberline {8}Zaklju\IeC {\v c}ak}{19}% 
\contentsline {chapter}{Literatura}{20}% 
